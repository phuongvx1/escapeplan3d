using System;
using UnityEngine;
using UnityEngine.Events;

public class Character : MonoBehaviour
{
    [SerializeField] private Team team;
    [SerializeField] private float maxHealth;
    [SerializeField] private float currentHealth;
    [SerializeField] private float mana;
    [SerializeField] private float baseDamage;
    [SerializeField] private float defend;
    
    public Action onHealthChanged;
    public UnityEvent onDead;

    public float BaseDamage => baseDamage;
    
    public float MaxHealth => maxHealth;
    
    public Team Team => team;
    
    public float CurrentHealth
    {
        get => currentHealth;
        set
        {
            currentHealth = value;
            onHealthChanged?.Invoke();
        }
    }

    public bool IsDead => CurrentHealth <= 0;

    private void Start() => CurrentHealth = maxHealth;

    public void TakeDamage(float damage)
    {
        if (IsDead) return;

        CurrentHealth -= damage;
        if (IsDead)
        {
            Die();
        }
    }

    protected virtual void Die() => onDead.Invoke();

    internal void TakeDamage(object damage)
    {
        throw new NotImplementedException();
    }
}
