using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class CharacterStatBar : MonoBehaviour
{
    [SerializeField] private Image healthValue;
    [SerializeField] private Character character;
    [SerializeField] private float visibleTime;

    private int firstFrame;
    private bool visibleOnHit;

    // private void Awake()
    // {
    //     gameObject.SetActive(false);
    // }

    private void OnEnable()
    {
        character.onHealthChanged += UpdateHealthValue;
        UpdateHealthValue();
        if (visibleOnHit)
        {
            gameObject.SetActive(false);
            firstFrame = Time.frameCount;
        }
    }

    private void OnDestroy() => character.onHealthChanged -= UpdateHealthValue;

    private void UpdateHealthValue()
    {
        healthValue.fillAmount = (int)character.CurrentHealth / character.MaxHealth;
        
        if (visibleOnHit && Time.frameCount != firstFrame)
        {
            StartCoroutine(ActiveHealthBar());
        }
    }

    private IEnumerator ActiveHealthBar()
    {
        gameObject.SetActive(true);
        yield return new WaitForSeconds(visibleTime);
        gameObject.SetActive(false);
    }
    
}
