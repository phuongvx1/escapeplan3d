using Monsters;
using TigerForge;
using UnityEngine;
using UnityEngine.AI;

public class EnemyAI : MonoBehaviour
{
    [SerializeField] private float lookRadius = 10f;
    [SerializeField] private Transform target;
    [SerializeField] private NavMeshAgent navMeshAgent;
    [SerializeField] private Vector3 followOffset;

    [SerializeField] private Transform[] wayPoints;
    [SerializeField] private float rotationDelay;
    
    private bool isChasing;
    private bool fisrtPatrol;
    
    private void Start()
    {
        SetupWayPoint();
        target = PlayerManager.instance.player.transform;
    }

    private void Update()
    {
        var distance = Vector3.Distance(target.position, transform.position);
        Moving(distance);
    }

    private void SetupWayPoint()
    {
        foreach (var wayPoint in wayPoints)
        {
            wayPoint.SetParent(null);
        }
    }
    
    private void Moving(float distance)
    {
        isChasing = distance <= lookRadius;
        if (isChasing)
        {
            Chasing(distance);
        }
        else
        {
            //Patrol(wayPoints);
        }
    }

    private void Chasing(float distance)
    {
        navMeshAgent.SetDestination(target.position - followOffset);

        if (distance <= navMeshAgent.stoppingDistance + followOffset.z)
        {
            FaceTarget();
        }
        else
        {
            EventManager.EmitEventData(MyEventManager.Enemy.animator,EnemyAnimatorState.ATTACK);
        }
    }

    private void Patrol(Transform[] currentWayPoints)
    {
        if (isChasing) return;
        
        EventManager.EmitEventData(MyEventManager.Enemy.animator,EnemyAnimatorState.WALK);

        if (fisrtPatrol)
        {
            var distance = Vector3.Distance(currentWayPoints[default].position, transform.position);
            navMeshAgent.SetDestination(currentWayPoints[default].position);

            if (distance <= navMeshAgent.stoppingDistance)
            {
                FaceTarget();
            }
        }
        else
        {
            var nextWayPoint = Random.Range(default,currentWayPoints.Length);
            var distance = Vector3.Distance(currentWayPoints[nextWayPoint].position, transform.position);
            navMeshAgent.SetDestination(currentWayPoints[nextWayPoint].position);

            if (distance <= navMeshAgent.stoppingDistance)
            {
                FaceTarget();
            }
        }
    }

    private void FaceTarget()
    {
        var direction = (target.position - transform.position).normalized;
        var lookRotation = Quaternion.LookRotation(new Vector3(direction.x,0,direction.z));
        transform.rotation = Quaternion.Slerp(transform.rotation,lookRotation,Time.deltaTime * rotationDelay);
        EventManager.EmitEventData(MyEventManager.Enemy.animator,EnemyAnimatorState.ATTACK);
    }
    
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position,lookRadius);
    }
}
