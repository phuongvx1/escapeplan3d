using System;
using System.Collections;
using DG.Tweening;
using Other;
using TigerForge;
using UnityEngine;

namespace Monsters
{
    public class EnemyAnimator : MonoBehaviour
    {
        private const string EnemyAttack = "attack";
        private const string EnemyIdle = "idle";
        private const string EnemyWalk = "walk";
        private const string EnemyDie = "die";
        
        [SerializeField] private Character character;
        [SerializeField] private Animator animator;
        
        private void OnEnable()
        {
            UpdateAnimator();
            EventManager.StartListening(MyEventManager.Enemy.animator,UpdateAnimator);
        }
        
        private void OnDisable()
        {
            EventManager.StopListening(MyEventManager.Enemy.animator,UpdateAnimator);
        }


        private void UpdateAnimator()
        {
            var nullableAnimator = EventManager.GetData(MyEventManager.Enemy.animator);

            if (nullableAnimator is EnemyAnimatorState stage)
            {
                if (character.IsDead) stage = EnemyAnimatorState.DIE;
                SwitchAnimator(stage);
            }
        }

        private void SwitchAnimator(EnemyAnimatorState state)
        {
            switch (state)
            {
                case EnemyAnimatorState.WALK:
                    animator.Play(EnemyWalk);
                    break;
                case EnemyAnimatorState.ATTACK:
                    animator.Play(EnemyAttack);
                    break;
                case EnemyAnimatorState.DIE:
                    StartCoroutine(DestroyCurrent.Instance.Object(gameObject, 1f));
                    animator.Play(EnemyDie);
                    break;
            }
        }
    }
    
    

    [Serializable]
    public enum EnemyAnimatorState
    {
        IDLE = 0,
        WALK = 1,
        ATTACK = 2,
        GETHIT = 3,
        DIE = 4
    }
}


