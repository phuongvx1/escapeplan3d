using UnityEngine;

namespace Monsters
{
    public class EnemyWeapon : MonoBehaviour
    {
        [SerializeField] private Enemy enemy;
        [SerializeField] private Character character;
        [SerializeField] private float damage;

        private float Damage => damage + character.BaseDamage;
        
        private void OnTriggerEnter(Collider other)
        {
            var targetCharacter = other.GetComponent<Character>();
            if (!targetCharacter) return;
            if (targetCharacter.Team == Team.Monster) return;

            targetCharacter.CurrentHealth -= Damage;
            Debug.Log(targetCharacter.CurrentHealth);
        }
    }

    public enum Enemy
    {
        Ghoul = 1,
    }
}