using System.Collections;
using System.Collections.Generic;
using Opsive.UltimateCharacterController.Items.Actions.Magic.CastActions;
using UnityEngine;

namespace Other
{
    public class DestroyCurrent : MonoBehaviour
    {
        public static DestroyCurrent Instance;

        void Awake ()
        {
            Instance = this;
        }
        
        public IEnumerator Object(GameObject gameObject,float dutation)
        {
            yield return new WaitForSeconds(dutation);
            gameObject.SetActive(false);
        }
    }
}
