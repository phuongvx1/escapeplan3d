using UnityEngine;


    [CreateAssetMenu(menuName = "Item/Item", fileName = nameof(Item))]
    public class Item : ScriptableObject
    {
        public ItemName itemName;
        public int quantity;
    }

