using System;
using UnityEngine;

public class Weapon : MonoBehaviour
{
    [SerializeField] private Weapones weapon;
    [SerializeField] private Character character;
    [SerializeField] private float damage;

    private float Damage => damage + character.BaseDamage;
    
    private void OnTriggerEnter(Collider other)
    {
        var targetCharacter = other.GetComponent<Character>();
        if (!targetCharacter) return;
        if (targetCharacter.Team == Team.Player) return;

        targetCharacter.CurrentHealth -= Damage;
    }
}

