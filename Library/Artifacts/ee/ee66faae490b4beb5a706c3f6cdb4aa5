                                          0.0.0 ţ˙˙˙      ˙˙f!ë59Ý4QÁóB   í          7  ˙˙˙˙                 Ś ˛                       E                    Ţ  #                     . ,                     5   a                    Ţ  #                     . ,                      r                    Ţ  #      	               . ,      
               H Ť ˙˙˙˙             1  1  ˙˙˙˙   @           Ţ                     Q  j                    ń  J   ˙˙˙˙   Ŕ           1  1  ˙˙˙˙               Ţ                       j  ˙˙˙˙               \     ˙˙˙˙               H r   ˙˙˙˙              1  1  ˙˙˙˙   @            Ţ                      Q  j                     H w   ˙˙˙˙              1  1  ˙˙˙˙   @            Ţ                      Q  j                     H    ˙˙˙˙              1  1  ˙˙˙˙   @            Ţ                      Q  j                     y 
                     Ţ  #      !               . ,      "                   ˙˙˙˙#   @          1  1  ˙˙˙˙$               Ţ      %               . j     &               Ő    ˙˙˙˙'               1  1  ˙˙˙˙(    Ŕ            Ţ      )                  j  ˙˙˙˙*                H   ˙˙˙˙+               1  1  ˙˙˙˙,   @            Ţ      -                Q  j     .                y 
    /                 Ţ  #      0               . ,      1                 §      2    @            ž ś      3    @            Ţ  #      4               . ,      5               H ť   ˙˙˙˙6              1  1  ˙˙˙˙7   @            Ţ      8                Q  j     9                H Ć   ˙˙˙˙:              1  1  ˙˙˙˙;   @            Ţ      <                Q  j     =                H Ř   ˙˙˙˙>              1  1  ˙˙˙˙?   @            Ţ      @                Q  j     A              MonoImporter PPtr<EditorExtension> m_FileID m_PathID PPtr<PrefabInstance> m_ExternalObjects SourceAssetIdentifier type assembly name m_UsedFileIDs m_DefaultReferences executionOrder icon m_UserData m_AssetBundleName m_AssetBundleVariant     s    ˙˙ŁGń×ÜZ56 :!@iÁJ*          7  ˙˙˙˙                 Ś ˛                        E                    Ţ                       .                      (   a                    Ţ                       .                       r                    Ţ        	               .       
               H Ť ˙˙˙˙             1  1  ˙˙˙˙   @           Ţ                     Q  j                    H ę ˙˙˙˙              1  1  ˙˙˙˙   @            Ţ                      Q  j                     ń  =   ˙˙˙˙              1  1  ˙˙˙˙               Ţ                       j  ˙˙˙˙               H   ˙˙˙˙              1  1  ˙˙˙˙   @            Ţ                      Q  j                     y 
                    Ţ                       .                      y Q                       Ţ                       .                       Ţ  X      !                H i   ˙˙˙˙"              1  1  ˙˙˙˙#   @            Ţ      $                Q  j     %                H u   ˙˙˙˙&              1  1  ˙˙˙˙'   @            Ţ      (                Q  j     )              PPtr<EditorExtension> m_FileID m_PathID PPtr<PrefabInstance> m_DefaultReferences m_Icon m_ExecutionOrder m_ClassName m_Namespace                        \       ŕyŻ     `       $                                                                                                                                                ŕyŻ                                                                                 
   CastAction  z  /// ---------------------------------------------
/// Ultimate Character Controller
/// Copyright (c) Opsive. All Rights Reserved.
/// https://www.opsive.com
/// ---------------------------------------------

namespace Opsive.UltimateCharacterController.Items.Actions.Magic.CastActions
{
    using Opsive.Shared.StateSystem;
    using Opsive.UltimateCharacterController.Utility;
    using UnityEngine;

    /// <summary>
    /// The Cast Action class performs the magic cast.
    /// </summary>
    [AllowDuplicateTypes]
    [UnityEngine.Scripting.Preserve]
    [System.Serializable]
    public abstract class CastAction : StateObject
    {
        [Tooltip("The delay to start the cast after the item has been used.")]
        [HideInInspector] [SerializeField] protected float m_Delay;

        public float Delay { get { return m_Delay; } set { m_Delay = value; } }
        public uint CastID { get { return m_CastID; } set { m_CastID = value; } }

        protected GameObject m_GameObject;
        protected MagicItem m_MagicItem;
        protected int m_Index;
        protected uint m_CastID;

        /// <summary>
        /// Initializes the CastAction.
        /// </summary>
        /// <param name="character">The character GameObject.</param>
        /// <param name="magicItem">The MagicItem that the CastAction belongs to.</param>
        /// <param name="index">The index of the CastAction.</param>
        public virtual void Initialize(GameObject character, MagicItem magicItem, int index)
        {
            base.Initialize(character);

            m_GameObject = character;
            m_MagicItem = magicItem;
            m_Index = index;
        }

        /// <summary>
        /// Awake is called after all of the actions have been initialized.
        /// </summary>
        public virtual void Awake() { }

        /// <summary>
        /// Is the specified position a valid target position?
        /// </summary>
        /// <param name="position">The position that may be a valid target position.</param>
        /// <param name="normal">The normal of the position.</param>
        /// <returns>True if the specified position is a valid target position.</returns>
        public virtual bool IsValidTargetPosition(Vector3 position, Vector3 normal) { return true; }

        /// <summary>
        /// Performs the cast.
        /// </summary>
        /// <param name="origin">The location that the cast should spawn from.</param>
        /// <param name="direction">The direction of the cast.</param>
        /// <param name="targetPosition">The target position of the cast.</param>
        public abstract void Cast(Transform origin, Vector3 direction, Vector3 targetPosition);

        /// <summary>
        /// The cast will be stopped. Start any cleanup.
        /// </summary>
        public virtual void WillStop() { }
        
        /// <summary>
        /// Stops the cast.
        /// </summary>
        /// <param name="castID">The ID of the cast that should be stopped.</param>
        public void Stop(uint castID) { m_CastID = castID; Stop(); }

        /// <summary>
        /// Stops the cast.
        /// </summary>
        public virtual void Stop() { m_CastID = 0; }

        /// <summary>
        /// The character has changed perspectives.
        /// </summary>
        /// <param name="origin">The location that the cast originates from.</param>
        public virtual void OnChangePerspectives(Transform origin) { }

        /// <summary>
        /// The action has been destroyed.
        /// </summary>
        public virtual void OnDestroy() { }
    }
}                      
   CastAction  B   Opsive.UltimateCharacterController.Items.Actions.Magic.CastActions  